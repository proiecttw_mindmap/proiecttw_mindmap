# ProiectTW_Mindmap

Operații la nivelul tabelei Articol
•	Inserare
	Pentru operatia de tip post,introducem URL-ul: http://localhost:3000/article/create

	Payload-ul pentru inserare este json-ul:
	{
	"title": "Untitled",
	"domain” : ”Other” ,
	"userId" : "1"
	} 

	Dacă înregistrarea va fi adăugată cu succes, va apărea mesajul ”Article created successfully!”,
	In caz contrar, va intoarce 500 si text-ul erorii aparute

•	Actualizare
	Pentru operatia de tip put, introducem URL-ul: http://localhost:3000/article/update1
	În acest caz se va actualiza înregistrarea ce va avea id-ul 1. 
	Înregistrarea se modifică tot printr-un fișier de tip JSON, ca la operația de inserare de mai sus.

	Dacă înregistrarea va fi adăugată cu succes, va apărea mesajul ”Article updated successfully!”,
	In caz contrar, va intoarce 500 si text-ul erorii aparute
	

•	Ștergere
	Pentru operatia de tip delete, introducem URL-ul http://localhost:3000/article/delete1
	În acest caz se va sterge înregistrarea ce va avea id-ul 1. 
	Nu este nevoie de payload.
	
	Dacă înregistrarea va fi adăugată cu succes, va apărea mesajul ”Article deleted successfully!”,
	In caz contrar, va intoarce 500 si text-ul erorii aparute

•	Selectare
	Pentru operatia de tip select introducem URL-ul http://localhost:3000/article/get
	Pentru aceasta operatie, se introduce in body un json sub forma:
	{
  	"userId" : "1"
	}, pentru care va intoarce articolele userului cu id 1




	

Operațiile pentru celelalte tabele vor urma aceiași pași, însă se va schimba URL-ul și fișierul json dupa cum urmeaza:

URL generic: http://localhost:3000/nume_tabela/tip_operatieid ( daca este necesar id-ul in cazul update sau delete) 
URL-ul va fi construit la fel ca mai sus, precum si mesajele intoarse, de succes sau eroare vor fi pe acelasi tipar.
Tabela user:
Pentru inserare si update va avea payload-ul:
	
	{
 	"username" : "sample",
        "password": "sample",
        "email": "email"
	}
Pentru stergere, precum la tabela articol, pe acelasi format.

Tabela nodemap:

Pentru inserare si update va avea payload-ul:
	
	{
 	"text" : "sample",
        "link_to_image": "link",
        "font": "font_name",
	"font_color":"font_color",
        "font_size":"font_size",
        "background_color":"background_color",
        "nodeMapId":"nodeMapId",
        "articleId":"articleId"
	}
Pentru stergere, precum la tabela articol, pe acelasi format.


Tabela bibliographicsource

Pentru inserare si update va avea payload-ul:
	
	{
 	title: "sample",
        "author":"name",
        "year": "year",
	"details": "details"
	}
	
Pentru stergere, precum la tabela articol, pe acelasi format.











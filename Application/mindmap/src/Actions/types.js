export const FETCH_AUT_DATA ='FETCH_AUT_DATA';
export const FETCH_AUT_DATA_ERR='FETCH_AUT_DATA_ERR';
export const TRIGGER_REGISTER='TRIGGER_REGISTER';
export const CANCEL_REGISTER='CANCEL_REGISTER';
export const FINISH_REGISTER='FINISH_REGISTER';
export const REGISTER_DUPLICATE_ERROR='REGISTER_DUPLICATE_ERROR';
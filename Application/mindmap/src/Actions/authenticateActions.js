import {FETCH_AUT_DATA,FETCH_AUT_DATA_ERR, TRIGGER_REGISTER,CANCEL_REGISTER, FINISH_REGISTER, REGISTER_DUPLICATE_ERROR} from './types';
import axios from 'axios';
export const fetchUser=(userSend)=>dispatch=>{
       
        axios.post('http://localhost:3000/user/login',userSend)
       
        .then(user=>
           { user.data!==''?
            
           dispatch({
            type:FETCH_AUT_DATA,
            payload:user.data
        }):dispatch({
                type:FETCH_AUT_DATA_ERR,
                payload:'err'
            })

            
    }).catch((err)=>{
            console.log(err)
        })   
}
 export const triggerRegister=()=>dispatch=>{
         dispatch({
                 type:TRIGGER_REGISTER,
                 payload:{}
         })
 }
 export const cancelRegister=()=>dispatch=>{
        dispatch({
                type:CANCEL_REGISTER,
                payload:{}
        })
}
export const createUser=(userSend)=>dispatch=>{

        axios.post('http://localhost:3000/user/register',userSend)
        
        .then(res =>
                
                dispatch({
                type:FINISH_REGISTER,
                payload:res.data,
                
        })).catch((err)=>{
                console.log(err)
                dispatch({
                        type:REGISTER_DUPLICATE_ERROR,
                        payload:'',
                        status:err.response.status
                })
            })   
        }
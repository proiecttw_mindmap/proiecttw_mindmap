import React, { Component } from 'react';
import './App.css';
import { Provider }from 'react-redux';
import store from'./store';
import RootPage from './Components/RootPage/RootPage';
import {BrowserRouter,Switch,Route} from 'react-router-dom';


class App extends Component {
  render() {
    
    return (
      
    <Provider store={store}>
    <BrowserRouter>

      <RootPage/>
    
      </BrowserRouter>
  </Provider>
    );
  }
}

export default App;
import {FETCH_AUT_DATA,FETCH_AUT_DATA_ERR,TRIGGER_REGISTER, CANCEL_REGISTER, FINISH_REGISTER, REGISTER_DUPLICATE_ERROR} from '../Actions/types';

const initialState={
    user:{},
    isAuthenticated:false,
    errAuthenticate:false,
    startRegister:false,
    finishRegister:'',
    duplicateUsername:0
}
export default function(state=initialState,action){
    switch(action.type){
        case FETCH_AUT_DATA:
             return {
            ...state,
            user:action.payload,
            isAuthenticated:true,
            startRegister:false,
            errAuthenticate:false
        }
        case FETCH_AUT_DATA_ERR:
            return{
                ...state,
            user:action.payload,
            isAuthenticated:false,
            startRegister:false,
            errAuthenticate:true
            }
        case TRIGGER_REGISTER:
            return{
                ...state,
                user:action.payload,
                isAuthenticated:false,
                startRegister:true,
                finishRegister:'',
                errAuthenticate:false
        }
        case CANCEL_REGISTER:
        return{
            ...state,
                user:action.payload,
                isAuthenticated:false,
                startRegister:false,
                finishRegister:'',
                errAuthenticate:false,
                duplicateUsername:0
        }
        case FINISH_REGISTER:
        return{
            ...state,
                user:{},
                isAuthenticated:false,
                startRegister:false,
                finishRegister:action.payload,
                errAuthenticate:false,
                
        }
        case REGISTER_DUPLICATE_ERROR:
        return{
            ...state,
                user:{},
                isAuthenticated:false,
                startRegister:true,
                finishRegister:action.payload,
                errAuthenticate:false,
                duplicateUsername:action.status
        }
        default:
        return state;
    }
}
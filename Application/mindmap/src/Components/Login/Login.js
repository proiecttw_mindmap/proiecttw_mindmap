import React, { Component } from 'react';
import './Login.css';
import {connect} from'react-redux';
import {fetchUser, triggerRegister } from '../../Actions/authenticateActions';
import PropTypes from'prop-types';
import {Link, withRouter} from'react-router-dom';
class Login extends Component {

 constructor(props){
   super(props);
   this.state={
     usernameForm:'',
     passwordForm:''
   }
   this.handleChange=this.handleChange.bind(this);
   this.handleRegister=this.handleRegister.bind(this);
   this.handleSubmit=this.handleSubmit.bind(this);
 }
  handleSubmit(e){
    e.preventDefault();
    const user={
      username:this.state.usernameForm,
      password:this.state.passwordForm}
    this.props.fetchUser(user);

    }
  
  handleChange(e){
    e.target.name==="username"?this.setState({usernameForm:e.target.value}):this.setState({passwordForm:e.target.value})
  }
  handleRegister(){
    this.props.triggerRegister();
  }



  render() {
    let eroare;
    if(this.props.errAuthenticate===true)
    eroare= <p className='eroareAuth'>Invalid username or password!</p>;
 
    return (
      <div className="login-page">
      
        <form className="form" onSubmit={this.handleSubmit}>

        
      <input type="text" placeholder="username" name="username" onChange={this.handleChange}/>
      <input type="password" placeholder="password" name="password" onChange={this.handleChange}/>
       <button type="submit" >login</button>
      <p className="message">Not registered? <Link to="" onClick={this.handleRegister} >Create an account</Link></p>
      {eroare}
    </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  errAuthenticate:state.loginReducer.errAuthenticate,
  isAuthenticated:state.loginReducer.isAuthenticated
});

export default connect(mapStateToProps,{fetchUser,triggerRegister})(Login);
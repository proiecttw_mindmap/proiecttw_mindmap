import React, { Component } from 'react'
import {connect} from'react-redux';
import Login from '../Login/Login';
import Register from '../Register/Register';
import './RootPage.css';
import WorkPage from '../WorkPage/WorkPage';
import { Switch, Route } from 'react-router-dom';
import {BrowserRouter}from 'react-router-dom';
 class RootPage extends Component {
    constructor(props){
      super(props);
    }
  
  render() {
    const yes= ()=>{alert(this.props.finishRegister)};
    if (this.props.startRegister===false&&this.props.finishRegister!==""&&
   this.props.isAuthenticated===false)
    yes();

    return (
     
     this.props.isAuthenticated?<WorkPage/>:this.props.startRegister?<Register/>:<Login/>
    )
  }
}
const mapStateToProps = state => ({
  isAuthenticated:state.loginReducer.isAuthenticated,
  startRegister:state.loginReducer.startRegister,
  finishRegister:state.loginReducer.finishRegister
});

export default connect(mapStateToProps)(RootPage);
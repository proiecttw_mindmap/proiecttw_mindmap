import React, { Component } from 'react'
import {cancelRegister,createUser } from '../../Actions/authenticateActions';
import {connect} from'react-redux';
import {Link,withRouter} from'react-router-dom';

 class Register extends Component {
  
constructor(props){
  super(props);
  this.state={
    usernameForm:'',
    passwordForm:'',
    emailForm:''
}
  this.handleOnClick=this.handleOnClick.bind(this);
  this.handleSubmit=this.handleSubmit.bind(this);
  this.handleOnChange=this.handleOnChange.bind(this);
}
handleOnClick(){
  this.props.cancelRegister();
}
handleSubmit(e){
  e.preventDefault();
  const user={
    username:this.state.usernameForm,
    password:this.state.passwordForm,
    email:this.state.emailForm
  }
  this.props.createUser(user);

}

handleOnChange(e){
  switch(e.target.name)
  {case "username" :
    this.setState({usernameForm:e.target.value})
    case "password":
    this.setState({passwordForm:e.target.value})
    default:
    this.setState({emailForm:e.target.value})
  }
}
  render() {
    let eroare;
    if(this.props.duplicateUsername===500)
    eroare=<p className="eroareAuth">Username already taken!</p>
    return (
        <div className="form">
        <form className="register-form" onSubmit={this.handleSubmit}>
          <input type="text" placeholder="username" name="username" onChange={this.handleOnChange}/>
          <input type="password" placeholder="password" name="password" onChange={this.handleOnChange}/>
          <input type="text" placeholder="email address" name="email" onChange={this.handleOnChange}/>
          <button>create</button>
          <p className="message">Already registered? <Link to="" onClick={this.handleOnClick}>Sign In</Link></p>
          {eroare}
        </form>
        </div>
    )
  }
}
const mapStateToProps = state => ({
  duplicateUsername:state.loginReducer.duplicateUsername
});
  export default connect(mapStateToProps,{cancelRegister,createUser})(Register);
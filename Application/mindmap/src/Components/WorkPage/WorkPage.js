import React, { Component } from 'react'
import Nod from '../Nod/Nod'
import DesignPage from '../DesignPage/DesignPage';
import {Link} from'react-router-dom';
import {connect} from'react-redux';
import axios from 'axios';

 class WorkPage extends Component {
   constructor(props){
     super(props);
     this.state={
       createClicked:false,
       //lista articole
       articole:[{
         idArticol:null,
         numeArticol:"",
       stareMindmap:{
         noduri:[
        
         ]
       }
      }],
      //articol curent
      articolCurent:{
        idArticolCurent:null,
        numeArticolCurent:"",
        noduriCurente:[
        ]
      }
     }

     this.handleOnClick=this.handleOnClick.bind (this);
     this.handleOnClickArticol=this.handleOnClickArticol.bind(this);
     this.handleOnChange=this.handleOnChange.bind(this);
   }
   //handler pt adaugarea unui nod nou
   onAddNod=(nod)=>{
     
   
    
     let nodBD={
       text:nod.text,
       nodeMapId:nod.idNodParinte,
       articleId:this.state.articolCurent.idArticolCurent
     }
axios.post('http://localhost:3000/node_maps/create',nodBD)
.then((res)=>
{  let noduri=this.state.articolCurent.noduriCurente;
  console.log(res.data)
  let nod={
    idNod:res.data.id,
    height:200,
            text:res.data.text,
            margin:'200px',
            idNodParinte:0
  }
  noduri.push(nod);
  this.setState({
   articolCurent:
   { idArticolCurent:this.state.articolCurent.idArticolCurent,
    numeArticolCurent:this.state.articolCurent.numeArticolCurent,
    noduriCurente:noduri

   }

   
  });
    
    let articles=this.state.articole;
    for(let i=0;i<articles.length;i++)
     {
      if(this.state.articolCurent.idArticolCurent==articles[i].idArticol)
      { 
        var noduri2=articles[i].stareMindmap.noduri;
      
  }
 }

 
});
   }
   //creeaza articol si salveaza in db
   handleOnClick(){
    this.setState({createClicked:true})
   
    const article={
      title:this.state.articolCurent.numeArticolCurent,
      userId:this.props.user.id
      
    };
    
    
    
    axios.post('http://localhost:3000/article/create',article)
    .then((res)=>
    {
      if(res.status===200){
       
        const nodNou=
          { 
            text:"nod nou",
          articleId:res.data.id,
          nodeMapId:null
          
          };
        
          //seteaza noduri curente pt art curent
          
         axios.post('http://localhost:3000/node_maps/create',nodNou)
         .then((result)=>
         {
            let noduri=[{idNod:result.data.id,
              text:"nod nou",
              height:200,
              margin:'200px',
            idNodParinte:null
              
            }];
            
            
            this.setState({
              articolCurent:{
              idArticolCurent:res.data.id,
              noduriCurente:noduri,
              numeArticolCurent:article.title
            }});
            let articole=this.state.articole;
            
            articole.push({
              
                idArticol:res.data.id,
                numeArticol:article.title,
              stareMindmap:{
                noduri:noduri}
              });
              
              this.setState({articole:articole});
           console.log(this.state.articole)
         })
         .catch((err)=>
         {console.log(err);
         });
         
      }
    })
    .catch((err=>
      console.log(err)));

   }
   //update stare in functie de valoarea din text box
   handleOnChange(e){
     this.setState({articolCurent:{
      idArticolCurent:null,
       numeArticolCurent:e.target.value,
       noduriCurente:[]
       
     }});
   }
   
   //incarca lista de articole din db
   componentWillMount(){
    
    
     axios.get(`http://localhost:3000/article/get${this.props.user.id}`)
     .then((res)=>
     {
       if(res.status===200){
         
     let articles=this.state.articole;
    
     
         for(let i=0;i<res.data.length;i++)
        {
        axios.get(`http://localhost:3000/node_maps/get${res.data[i].id}`)
        .then((result)=>
        {var noduri=[];
          for(let j=0;j<result.data.length;j++)
          {
            let nod={
              idNod:result.data[j].id,
              height:200,
              text:result.data[j].text,
              margin:'200px',
             idNodParinte:result.data[j].nodeMapId

            }
            
            noduri.push(nod);
          }
          let articol={
            idArticol:res.data[i].id,
            numeArticol:res.data[i].title,
            stareMindmap:{
              noduri:noduri
            }
            
          }
          
          articles.push(articol);
          this.setState({articole:articles});
        })
        .catch((err)=>{
          console.log(err)
        });
        
        }
       
        
      }
     })
     .catch((err)=>
     console.log(err));
   }
   //incarca articolul curent cand se da click pe un item din lista
   handleOnClickArticol(e){
     let articole=this.state.articole;
     
     for(let i=0;i<articole.length;i++)
     {
       if(articole[i].idArticol==e)
       {
         this.setState({
           articolCurent:{
             idArticolCurent:articole[i].idArticol,
             numeArticolCurent:articole[i].numeArticol,
             noduriCurente:articole[i].stareMindmap.noduri
           },
           createClicked:true

         })
       }
     }
    
   }
  render() {
    
    var articole = this.state.articole.map((articole, index) =>{
      return <div key={index} ref={index}>< Link to="/articol" onClick={()=>this.handleOnClickArticol(articole.idArticol)}>{articole.idArticol}-{articole.numeArticol}</Link></div>
  })
    if( this.state.createClicked==false)
       
        return <div>
       
      <input className="form-control mr-sm-2" type="text" placeholder="Article name"  onChange={this.handleOnChange}></input>
      <button type="button" className="btn btn-primary" onClick={this.handleOnClick}>Create article</button>
      
  
<p>Lista articole</p>

{articole}
</div>
        else

        return <div><DesignPage stareMindmap={this.state.articolCurent.noduriCurente} addNod={this.onAddNod} />

</div>
      
    
  }
}
const mapStateToProps = state => ({

  user:state.loginReducer.user
});
export default connect(mapStateToProps,null)(WorkPage)
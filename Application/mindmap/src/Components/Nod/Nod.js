import React, { Component } from 'react'

 class Nod extends Component {
  constructor(props){
    super(props);
    this.state={
      input:'',
      left : 100
    }
    this.handleChange=this.handleChange.bind(this);
    }



handleChange(e){
  
this.setState({input:e.target.value});
let nodeData={
  id:this.props.nodCurent.idNod,
  text:e.target.value
};

this.props.functieSave(nodeData);
    }
  render() {
    
    return (
     
       <div height={this.props.nodCurent.height} >
       
       <svg width="100%"  xmlns="http://www.w3.org/2000/svg">
         <line id="l1" x1="100" y1="40" x2="100" y2="200"></line>
         <line id="l2" x1="80" y1="80" x2="100" y2="150"></line>
         <line id="l3" x1="120" y1="80" x2="100" y2="150"></line>
      <foreignObject x="10" y="10" width="100" height="100">
      <div xmlns="http://www.w3.org/1999/xhtml">
      <input type="text" placeholder={this.props.nodCurent.text} onChange={this.handleChange}/>
     
          </div>
  </foreignObject>
</svg>
 
</div>
    )
  }
}
export default Nod
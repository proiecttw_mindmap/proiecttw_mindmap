import React, { Component } from 'react'
import Nod from '../Nod/Nod'
import axios from 'axios';


 class DesignPage extends Component {
     constructor(props)
     {super(props);
        this.state={
            
        }
        this.handleOnClick=this.handleOnClick.bind(this);
     }
     handleOnClick(){
      
         
         let nodNou={
         idNod:null,
         height:200,
         text:"",
         margin:'200px',
         idNodParinte:null
        }
        
         this.props.addNod(nodNou);

     }
  //functie salvare stare mindmpa

  functieSave= (nodeData)=>{
    
    for(let i=0;i<this.props.stareMindmap.length;i++)
    {
      if(nodeData.id==this.props.stareMindmap[i].idNod)
      {
        let body={
          text:nodeData.text
        }
      
         axios.put(`http://localhost:3000/node_maps/update${this.props.stareMindmap[i].idNod}`,body)
         .then((res)=>
         {
           console.log(res.data);
         });
        
      }
    }
  }
  
    
  render() {
    
      let incarcaMindmap=this.props.stareMindmap.map((nod,index)=>{
      
      return <Nod key={index} nodCurent={nod}  functieSave={this.functieSave}/>
  });
      
    return (
      
       
       <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <a className="navbar-brand" href="#">Create your chain-map!</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <a className="nav-link" href="#" onClick={this.handleOnClick}>Add <span className="sr-only">(current)</span></a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#" >AutoSave:ON</a>
      </li>
     
    </ul>
    
  </div>
  
</nav>

{incarcaMindmap}


       </div>
 
      
    )
  }
}
export default DesignPage
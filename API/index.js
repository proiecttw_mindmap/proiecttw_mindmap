const express = require('express')
const bodyParser = require('body-parser')
const {User,Article, NodeMap,BibliographicSource, NodeSource} = require('./sequelize')
const cors = require('cors')

const app = express()
app.use(cors());
app.use(bodyParser.json())

// API ENDPOINTS

const port = 3000;


//User operations
// login
app.post('/user/login', (req, res) => {
    User.findOne({where:{username: req.body.username, password: req.body.password} }).then((result) => {
        res.status(200).send(result)
        console.log(result)
    }) 
 });
 //register
 app.post('/user/register', (req, res) =>{
    User.create({
        username: req.body.username,
        password: req.body.password,
        email:req.body.email
    }).then(() => {
        res.status(200).send("User created successfully");
    }, (err) => {
        res.status(500).send(err);
    })
});
//update pass
app.put('/user/update:id', (req,res) => 
{
    User.update({
        
        password:req.body.password,
        
    }, {where: {
        id:req.params.id}
    }
    ).then(()=> {
        res.status(200).send("Password updated succesfully");
    }, (err) =>
    {
        res.status(500).send(err);
    })
})
//Article operations
app.post('/article/create', (req,res) => 
{
    Article.create({
        title: req.body.title,
        domain: req.body.domain,
        userId:req.body.userId
    }).then((article)=> {
        res.status(200).send(article);
    }, (err) =>
    {
        res.status(500).send(err);
    })
});

app.put('/article/update:id', (req,res) => 
{
    Article.update({
        title: req.body.title,
        domain: req.body.domain,
        userId: req.body.userId
    }, {where: {
        id:req.params.id}
    }
    ).then(()=> {
        res.status(200).send("Article updated successfully!");
    }, (err) =>
    {
        res.status(500).send(err);
    })
})
app.delete('/article/delete:id', (req,res) => 
{
    Article.destroy(
         {where: {
        id:req.params.id}
    }
    ).then(()=> {
        res.status(200).send("Article deleted successfully!");
    }, (err) =>
    {
        res.status(500).send(err);
    })
})

app.get('/article/get:userId', (req, res) => {
    Article.findAll({where:{userId:req.params.userId} })
    .then((result) => {
        res.status(200).send(result)
        console.log(result)
    }) 
 });
//node_maps
app.post('/node_maps/create', (req,res) => 
{
    NodeMap.create({
        text:req.body.text,
        link_to_image:req.body.link_to_image,
        font:req.body.font,
        font_color:req.body.font_color,
        font_size:req.body.font_size,
        background_color:req.body.background_color,
        nodeMapId:req.body.nodeMapId,
        articleId:req.body.articleId
    }).then((node)=> {
        res.status(200).send(node);
    })
});
app.put('/node_maps/update:id', (req,res) => 
{
    NodeMap.update({
        text:req.body.text,
        link_to_image:req.body.link_to_image,
        font:req.body.font,
        font_color:req.body.font_color,
        font_size:req.body.font_size,
        background_color:req.body.background_color,
        nodeMapId:req.body.nodeMapId,
        articleId:req.body.articleId
    }, {
        where: {
        id:req.params.id}
    }
    ).then(()=> {
        res.status(200).send("Node updated successfully!");
    })
});
app.delete('/node_maps/delete:id', (req,res) => 
{
    NodeMap.destroy(
        {where: {
            id:req.params.id}
        }
        ).then(()=> {
            res.status(200).send("Node deleted successfully!");
        })
});
app.get('/node_maps/get:articleId', (req, res) => {
    NodeMap.findAll({where:{articleId:req.params.articleId} })
    .then((result) => {
        res.status(200).send(result)
        console.log(result)
    }) 
 });
//bibliographic Source operations
app.post('/bibliographicsource/create', (req,res) => 
{
    BibliographicSource.create({
        title: req.body.title,
        author: req.body.author,
        year:req.body.year,
        details:req.body.details
    }).then(()=> {
        res.status(200).send("Bibliographic source created successfully!");
    }, (err) =>
    {
        res.status(500).send(err);
    })
});
app.put('/bibliographicsource/update:id', (req,res) => 
{
    BibliographicSource.update({
        title: req.body.title,
        author: req.body.author,
        year:req.body.year,
        details:req.body.details
    }, {where: {
        id:req.params.id}
    }
    ).then(()=> {
        res.status(200).send("Bibliographic source updated successfully!");
    }, (err) =>
    {
        res.status(500).send(err);
    })
});
app.delete('/bibliographicsource/delete:id', (req,res) => 
{
    BibliographicSource.destroy(
         {where: {
        id:req.params.id}
    }
    ).then(()=> {
        res.status(200).send("Bibliographic source deleted successfully!");
    }, (err) =>
    {
        res.status(500).send(err);
    })
})
app.get('/bibliographicsource/get', (req, res) => {
    NodeSource.findAll({
        where:{nodeMapId:req.body.nodeMapId},
        include:[{
            model:BibliographicSource,
           
        }]
         })
    .then((result) => {
        res.status(200).send(result)
        console.log(result)
    }) 
 });


app.listen(port, () => {
    console.log(`Running on http://localhost:${port}`)
});
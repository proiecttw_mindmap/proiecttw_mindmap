const Sequelize = require('sequelize');

const UserModel = require('./models/user');
const ArticleModel = require('./models/article');
const NodeMapModel = require('./models/nodeMap');
const BibliographicSourceModel = require('./models/bibliographicSource');
const NodeSourceModel = require('./models/nodeSource');
const sequelize = new Sequelize('mindmap', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
      max: 10,
      min: 0,
      idle: 20000,
      acquire: 20000
     
    },
   
  })
  const User = UserModel(sequelize, Sequelize);
  const Article = ArticleModel(sequelize, Sequelize);
  Article.belongsTo(User);
  const NodeMap = NodeMapModel(sequelize, Sequelize);
  NodeMap.belongsTo(NodeMap);
  NodeMap.belongsTo(Article);
  const BibliographicSource = BibliographicSourceModel(sequelize, Sequelize);
  const NodeSource = NodeSourceModel(sequelize,Sequelize);
  NodeSource.belongsTo(NodeMap);
  NodeSource.belongsTo(BibliographicSource);


  sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  sequelize.sync({ force: true })
  .then(() => {
   console.log(`Database & tables created!`)
  }).catch((err)=>{console.log('Failed to create tables');})
 

module.exports = {
  User,
  Article,
  NodeMap,
  BibliographicSource,
  NodeSource
}
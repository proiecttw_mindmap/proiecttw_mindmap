module.exports = (sequelize,type) => {
    return sequelize.define('node_map', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        text: {
            type: type.STRING,
            allowNull:true
        },
        link_to_image: {
            type: type.STRING,
            allowNull:true 
        },
        font: {
            type: type.STRING,
            allowNull:true
        },
        font_color: {
            type: type.STRING,
            allowNull:true
        },
        font_size: {
            type: type.INTEGER,
            allowNull:true
        },
        background_color: {
            type: type.STRING,
            allowNull:true
        }
    })
}
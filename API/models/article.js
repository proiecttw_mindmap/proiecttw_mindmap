module.exports = (sequelize,type) => {
    return sequelize.define('article', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        title: {
            type: type.STRING,
            allowNull:false
        },
        domain: {
            type: type.STRING,
            allowNull:true 
        }
    })
}
module.exports = (sequelize,type) => {
    return sequelize.define('bibliographic_source', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        title: {
            type: type.STRING,
            allowNull:false
        },
        author: {
            type: type.STRING,
            allowNull:false 
        },
        year: {
            type: type.INTEGER,
            allowNull:true
        },
        details: {
            type: type.STRING,
            allowNull:true
        }
    })
}